**Money transfer service**

**Build**  
`mvn clean install` will generate a runnable jar in the /target directory.

**Run**  
`cd target`  
`java -jar revolut-coding-task-1.0-SNAPSHOT.jar`
After this requests can be send to the endpoint on _localhost:8000/transfer_

**Tools used**  
_Java 11_  
_Javalin_ - web framework  
_JOOQ_ - JDBC library for transaction management and simple SQL commands handling  
_Junit, Mockito, AssertJ, Unirest_ - for testing
_H2 Database_ - in memory store


**REST API**  
POST request to _/transfer_ endpoint  
JSON example is pretty self-describing except for the "amountMicros"
```
{"fromAccountId" : 1,
 "toAccountId" : 2,
 "amountMicros" : 111111}
```

In order to simplify money calculations 
it was decided to use micros (1 millionth part of a money unit) as a convention. 
So if a user wants to transfer 1 dollar "amountMicros" should be 1000000.

After a successful transfer response 200 is sent and the response JSON object that looks like this
```
{
    "transfer": {
        "fromAccountId": 1,
        "toAccountId": 2,
        "amountMicros": 111111
    },
    "fromAccountBalanceMicrosAfterTransfer": 888889
}
```

On client error (e.g. nonexistent account, not enough money etc) 
response 400 us sent and the response JSON object that looks like this
```
{
    "errorMessage": "Account with id 100 does not exist."
}
```

**Implementation notes**  
I tried to keep it as simple as possible. 
In the real world request would include correlation id and some other fields. 
Transactions are used to make sure that money transfer is consistent.   
It was important to make sure that deadlocks can't happen.  
Only 1 table in DB is used with 2 columns: ID and BALANCE_MICROS.  
Regarding scalability - the service is stateless, so it can be scaled horizontally (i.e create more instances if needed).  
Potential bottleneck in scalability is a database. Every transaction is blocking 2 entries to update corresponding balances 
which is OK considered that transfers are done mostly between different accounts which don't interfere with one another.

**What can be improved**  
To make this service more scalable, async requests processing can be implemented.
Both JOOQ and Javalin libraries support this.  
In the real world database connection pool should be used.  
REST API can be documented using Swagger or alternative tools.  