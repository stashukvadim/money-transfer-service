package com.github.stashukvadim.revolut.transfer.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Transfer {
    private final long fromAccountId;
    private final long toAccountId;
    private final long amountMicros;

    /*JsonCreator and JsonProperty annotations are needed to ensure, that all properties are present in the JSON*/
    @JsonCreator
    public Transfer(
            @JsonProperty(value = "fromAccountId", required = true) long fromAccountId,
            @JsonProperty(value = "toAccountId", required = true) long toAccountId,
            @JsonProperty(value = "amountMicros", required = true) long amountMicros) {
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.amountMicros = amountMicros;
    }

    public long getFromAccountId() {
        return fromAccountId;
    }

    public long getToAccountId() {
        return toAccountId;
    }

    public long getAmountMicros() {
        return amountMicros;
    }

    @Override
    public String toString() {
        return "Transfer{" +
                "fromAccountId=" + fromAccountId +
                ", toAccountId=" + toAccountId +
                ", amountMicros=" + amountMicros +
                '}';
    }
}
