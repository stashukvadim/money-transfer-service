package com.github.stashukvadim.revolut.transfer.model;

public class Account {
    private final long id;
    private final long balanceMicros;

    public Account(long id, long balanceMicros) {
        this.id = id;
        this.balanceMicros = balanceMicros;
    }

    public long getId() {
        return id;
    }

    public long getBalanceMicros() {
        return balanceMicros;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", balanceMicros=" + balanceMicros +
                '}';
    }
}
