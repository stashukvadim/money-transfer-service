package com.github.stashukvadim.revolut.transfer.service;

import com.github.stashukvadim.revolut.transfer.TransferException;
import com.github.stashukvadim.revolut.transfer.model.Account;
import com.github.stashukvadim.revolut.transfer.model.Transfer;
import com.github.stashukvadim.revolut.transfer.model.TransferResult;
import com.github.stashukvadim.revolut.transfer.repository.AccountRepository;
import com.github.stashukvadim.revolut.transfer.repository.TransactionUtil;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

import static java.lang.String.format;

public class TransferService {
    private final AccountRepository accountRepo;
    private final TransactionUtil transactionUtil;

    public TransferService(AccountRepository accountRepo, TransactionUtil transactionUtil) {
        this.accountRepo = accountRepo;
        this.transactionUtil = transactionUtil;
    }

    public TransferResult transfer(Transfer transfer) {
        Objects.requireNonNull(transfer);

        //use atomic long to update non-final variable from lambda expression
        //this variable is needed fro the TransferResult object
        AtomicLong fromAccountBalanceAfterTransfer = new AtomicLong();
        try {
            transactionUtil.doInTransaction(() -> {
                if (transfer.getFromAccountId() == transfer.getToAccountId()) {
                    throw new TransferException("Transfer between the same account is not allowed.");
                }
                if (transfer.getAmountMicros() <= 0) {
                    throw new TransferException(format("Amount should be greater than 0. Actual value = %s.", transfer.getAmountMicros()));
                }

                Account fromAccount;
                Account toAccount;
                //to prevent dead-locks we need to get accounts using a consistent logic
                //in this case we always select the account with smaller id first
                if (transfer.getFromAccountId() < transfer.getToAccountId()) {
                    fromAccount = getAccount(transfer.getFromAccountId());
                    toAccount = getAccount(transfer.getToAccountId());
                } else {
                    toAccount = getAccount(transfer.getToAccountId());
                    fromAccount = getAccount(transfer.getFromAccountId());
                }

                fromAccountBalanceAfterTransfer.set(fromAccount.getBalanceMicros() - transfer.getAmountMicros());
                if (fromAccountBalanceAfterTransfer.get() < 0) {
                    throw new TransferException(format("Account with id %s doesn't have enough money to make the transfer.", fromAccount.getId()));
                }

                checkToAccountForOverflow(toAccount, transfer);

                accountRepo.addAmountMicros(fromAccount.getId(), -transfer.getAmountMicros());
                accountRepo.addAmountMicros(toAccount.getId(), transfer.getAmountMicros());
            });
        } catch (TransferException te) {
            throw te;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return new TransferResult(transfer, fromAccountBalanceAfterTransfer.get());
    }

    /**
     * This is to check that the toAccountBalanceMicros after transfer is not greater than long max value.
     * This is highly unlikely because max supported balance is around 9 trillion dollars (or other currency)
     */
    private void checkToAccountForOverflow(Account toAccount, Transfer transfer) {
        try {
            Math.addExact(toAccount.getBalanceMicros(), transfer.getAmountMicros());
        } catch (ArithmeticException e) {
            throw new TransferException("toAccount can't accept this transaction.");
        }
    }

    private Account getAccount(long id) {
        return accountRepo.getAccount(id)
                .orElseThrow(() -> new TransferException(format("Account with id %s does not exist.", id)));
    }
}
