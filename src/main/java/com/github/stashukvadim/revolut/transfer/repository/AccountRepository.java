package com.github.stashukvadim.revolut.transfer.repository;

import com.github.stashukvadim.revolut.transfer.model.Account;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;

import java.util.Optional;

import static org.jooq.impl.DSL.sql;

public class AccountRepository {
    private final DSLContext ctx;

    public AccountRepository(TransactionUtil transactionUtil) {
        ctx = transactionUtil.getCtx();
    }

    public Optional<Account> getAccount(long id) {
        Result<Record> result = ctx.fetch(sql("SELECT balance_micros FROM Accounts WHERE id = ? FOR UPDATE", id));
        if (result.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(new Account(id, (Long) result.getValue(0, 0)));
    }

    public void addAmountMicros(long id, long amountToAdd) {
        ctx.execute(sql("UPDATE Accounts SET balance_micros = balance_micros + ? WHERE id = ?", amountToAdd, id));
    }
}
