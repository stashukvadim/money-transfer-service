package com.github.stashukvadim.revolut.transfer;

import com.github.stashukvadim.revolut.transfer.model.Transfer;
import com.github.stashukvadim.revolut.transfer.service.TransferService;
import io.javalin.Javalin;
import org.eclipse.jetty.http.HttpStatus;

public class Application {
    private final int port;
    private final TransferService transferService;

    public Application(int port, TransferService transferService) {
        this.port = port;
        this.transferService = transferService;
    }

    public Javalin startApplication() {
        Javalin app = Javalin.create().start(port);
        app.post("/transfer", ctx -> ctx.json(transferService.transfer(ctx.bodyAsClass(Transfer.class))));
        app.exception(TransferException.class, (e, ctx) -> ctx.status(HttpStatus.BAD_REQUEST_400).json(e.getClientError()));

        return app;
    }
}
