package com.github.stashukvadim.revolut.transfer.model;

public class TransferResult {
    private final Transfer transfer;
    private final long fromAccountBalanceMicrosAfterTransfer;

    public TransferResult(Transfer transfer, long fromAccountBalanceMicrosAfterTransfer) {
        this.transfer = transfer;
        this.fromAccountBalanceMicrosAfterTransfer = fromAccountBalanceMicrosAfterTransfer;
    }

    public Transfer getTransfer() {
        return transfer;
    }


    public long getFromAccountBalanceMicrosAfterTransfer() {
        return fromAccountBalanceMicrosAfterTransfer;
    }

}
