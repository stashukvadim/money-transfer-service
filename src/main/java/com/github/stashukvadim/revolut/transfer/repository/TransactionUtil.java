package com.github.stashukvadim.revolut.transfer.repository;

import org.h2.jdbcx.JdbcDataSource;
import org.jooq.ContextTransactionalRunnable;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.ThreadLocalTransactionProvider;

public class TransactionUtil {
    private final DSLContext ctx;

    public TransactionUtil(String url) {
        DefaultConfiguration configuration = new DefaultConfiguration();
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL(url);
        configuration.setTransactionProvider(new ThreadLocalTransactionProvider(new DataSourceConnectionProvider(dataSource)));
        ctx = DSL.using(configuration);
    }

    public void doInTransaction(ContextTransactionalRunnable transactionalRunnable) {
        ctx.transaction(transactionalRunnable);
    }

    DSLContext getCtx() {
        return ctx;
    }
}
