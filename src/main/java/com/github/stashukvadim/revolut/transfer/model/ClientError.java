package com.github.stashukvadim.revolut.transfer.model;

public class ClientError {
    private String errorMessage;

    public ClientError(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}