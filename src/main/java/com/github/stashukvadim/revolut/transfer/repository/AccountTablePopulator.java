package com.github.stashukvadim.revolut.transfer.repository;

import org.jooq.DSLContext;

import static org.jooq.impl.DSL.sql;

/**
 * This is used to add a table and users. Needed for testing and for the main application to demonstrate how it works.
 * However if it was used in tests only - it would be in a test package available only in test scope.
 */
public class AccountTablePopulator {
    private final DSLContext ctx;

    public AccountTablePopulator(TransactionUtil transactionUtil) {
        ctx = transactionUtil.getCtx();
    }

    public void createAccountsTableIfNotExists() {
        ctx.execute("CREATE TABLE IF NOT EXISTS Accounts " +
                "(id BIGINT NOT NULL, " +
                " balance_micros BIGINT NOT NULL, " +
                " PRIMARY KEY (id))");
    }

    public void addAccount(long id, long initialBalanceMicros) {
        ctx.execute(sql("INSERT INTO Accounts VALUES (?, ?)", id, initialBalanceMicros));
    }

    public void deleteAllAccounts() {
        ctx.execute("DELETE FROM Accounts");
    }
}
