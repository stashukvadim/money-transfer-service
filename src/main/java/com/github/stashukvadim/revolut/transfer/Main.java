package com.github.stashukvadim.revolut.transfer;

import com.github.stashukvadim.revolut.transfer.repository.AccountRepository;
import com.github.stashukvadim.revolut.transfer.repository.AccountTablePopulator;
import com.github.stashukvadim.revolut.transfer.repository.TransactionUtil;
import com.github.stashukvadim.revolut.transfer.service.TransferService;

public class Main {
    public static void main(String[] args) {
        //in a real life db url and port would be configurable
        final String dbUrl = "jdbc:h2:mem:example_db;DB_CLOSE_DELAY=-1";
        final int port = 8000;

        TransactionUtil transactionUtil = new TransactionUtil(dbUrl);
        createDbTableAndPopulateWithTestData(transactionUtil);

        AccountRepository accountRepo = new AccountRepository(transactionUtil);
        TransferService transferService = new TransferService(accountRepo, transactionUtil);

        new Application(port, transferService).startApplication();
    }

    private static void createDbTableAndPopulateWithTestData(TransactionUtil transactionUtil) {
        AccountTablePopulator accountTablePopulator = new AccountTablePopulator(transactionUtil);
        accountTablePopulator.createAccountsTableIfNotExists();
        accountTablePopulator.addAccount(1, 1_000_000);
        accountTablePopulator.addAccount(2, 2_000_000);
        accountTablePopulator.addAccount(3, 3_000_000);
        accountTablePopulator.addAccount(4, 5_000_000);
    }
}
