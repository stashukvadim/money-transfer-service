package com.github.stashukvadim.revolut.transfer;

import com.github.stashukvadim.revolut.transfer.model.ClientError;

public class TransferException extends RuntimeException {
    public TransferException(String s) {
        super(s);
    }

    public ClientError getClientError() {
        return new ClientError(getMessage());
    }
}
