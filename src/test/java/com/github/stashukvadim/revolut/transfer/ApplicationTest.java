package com.github.stashukvadim.revolut.transfer;

import com.github.stashukvadim.revolut.transfer.repository.AccountRepository;
import com.github.stashukvadim.revolut.transfer.repository.AccountTablePopulator;
import com.github.stashukvadim.revolut.transfer.repository.TransactionUtil;
import com.github.stashukvadim.revolut.transfer.service.TransferService;
import com.github.stashukvadim.revolut.transfer.testutil.TransferTestUtil;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ApplicationTest {
    private static final int PORT = 8001;
    private static final String URL = "http://localhost:8001/transfer";

    private static TransactionUtil transactionUtil = TransferTestUtil.getTransactionUtil();
    private static AccountRepository accountRepository = new AccountRepository(transactionUtil);
    private static TransferService transferService = new TransferService(accountRepository, transactionUtil);
    private static Application application = new Application(PORT, transferService);
    private static AccountTablePopulator accountTablePopulator = new AccountTablePopulator(transactionUtil);

    @BeforeClass
    public static void startApp() {
        application.startApplication();
        accountTablePopulator.createAccountsTableIfNotExists();
    }

    @Before
    public void setUp() {
        accountTablePopulator.deleteAllAccounts();
    }

    @Test
    public void successTransfer() throws Exception {
        accountTablePopulator.addAccount(1, 1_000_000);
        accountTablePopulator.addAccount(2, 2_000_000);

        var response = Unirest.post(URL).body(jsonString(1, 2, 1_000_000)).asJson();

        assertResponseStatusOk(response);
        assertFromAccountBalanceMicrosAfterTransferIsEqualTo(response, 0);

        JSONObject transfer = response.getBody().getObject().getJSONObject("transfer");
        assertThat(transfer.getLong("fromAccountId")).isEqualTo(1);
        assertThat(transfer.getLong("toAccountId")).isEqualTo(2);
        assertThat(transfer.getLong("amountMicros")).isEqualTo(1_000_000);
    }


    @Test
    public void multipleSuccessTransfers() throws Exception {
        accountTablePopulator.addAccount(1, 1_000_000);
        accountTablePopulator.addAccount(2, 2_000_000);
        accountTablePopulator.addAccount(3, 3_000_000);

        var responseFrom1_to_2 = Unirest.post(URL).body(jsonString(1, 2, 1_000_000)).asJson();
        assertResponseStatusOk(responseFrom1_to_2);
        assertFromAccountBalanceMicrosAfterTransferIsEqualTo(responseFrom1_to_2, 0);

        var responseFrom3_to_1 = Unirest.post(URL).body(jsonString(3, 1, 2_000_000)).asJson();
        assertResponseStatusOk(responseFrom3_to_1);
        assertFromAccountBalanceMicrosAfterTransferIsEqualTo(responseFrom3_to_1, 1_000_000);


        responseFrom1_to_2 = Unirest.post(URL).body(jsonString(1, 2, 500_000)).asJson();
        assertResponseStatusOk(responseFrom1_to_2);
        assertFromAccountBalanceMicrosAfterTransferIsEqualTo(responseFrom1_to_2, 1_500_000);
    }

    @Test
    public void transferWithToAccountOverflow() throws Exception {
        accountTablePopulator.addAccount(1, 1_000_000);
        accountTablePopulator.addAccount(2, Long.MAX_VALUE);

        var response = Unirest.post(URL).body(jsonString(1, 2, 1)).asJson();
        JSONObject responseJsonObject = response.getBody().getObject();

        assertResponseStatusBadRequest(response);
        assertThat(responseJsonObject.getString("errorMessage")).isEqualTo("toAccount can't accept this transaction.");
    }

    @Test
    public void transferWithInsufficientFunds() throws Exception {
        accountTablePopulator.addAccount(1, 1_000_000);
        accountTablePopulator.addAccount(2, 2_000_000);

        var response = Unirest.post(URL).body(jsonString(1, 2, 1_000_001)).asJson();
        JSONObject responseJsonObject = response.getBody().getObject();

        assertResponseStatusBadRequest(response);
        assertThat(responseJsonObject.getString("errorMessage")).isEqualTo("Account with id 1 doesn't have enough money to make the transfer.");
    }

    @Test
    public void transferWithNonExistentAccount() throws Exception {
        var response = Unirest.post(URL).body(jsonString(1111, 2222, 1_000_000)).asJson();
        JSONObject responseJsonObject = response.getBody().getObject();

        assertResponseStatusBadRequest(response);
        assertThat(responseJsonObject.getString("errorMessage")).isEqualTo("Account with id 1111 does not exist.");
    }

    @Test
    public void transferWithInvalidJson() throws Exception {
        String jsonWithoutLastParentheses = "{\"fromAccountId\" : 1111, \"toAccountId\" : 22222, \"amountMicros\" : 1000000";
        var response = Unirest.post(URL).body(jsonWithoutLastParentheses).asString();

        assertResponseStatusBadRequest(response);
        assertThat(response.getBody()).isEqualTo("Couldn't deserialize body to Transfer");
    }

    @Test
    public void transferWithNotAllRequiredFieldsPresent() throws Exception {
        String jsonWithoutAmount = "{\"fromAccountId\" : 1111, \"toAccountId\" : 22222}";
        var response = Unirest.post(URL).body(jsonWithoutAmount).asString();

        assertResponseStatusBadRequest(response);
        assertThat(response.getBody()).isEqualTo("Couldn't deserialize body to Transfer");
    }

    private String jsonString(long fromAccountId, long toAccountId, long amountMicros) {
        return String.format("{\"fromAccountId\" : %s, \"toAccountId\" : %s, \"amountMicros\" : %s}", fromAccountId, toAccountId, amountMicros);
    }

    private void assertResponseStatusOk(HttpResponse<?> httpResponse) {
        assertThat(httpResponse.getStatus()).isEqualTo(HttpStatus.OK_200);
    }

    private void assertResponseStatusBadRequest(HttpResponse<?> httpResponse) {
        assertThat(httpResponse.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST_400);
    }

    private void assertFromAccountBalanceMicrosAfterTransferIsEqualTo(HttpResponse<JsonNode> response, long expected) {
        assertThat(response.getBody().getObject().getLong("fromAccountBalanceMicrosAfterTransfer")).isEqualTo(expected);
    }
}