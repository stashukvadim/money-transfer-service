package com.github.stashukvadim.revolut.transfer.service;

import com.github.stashukvadim.revolut.transfer.TransferException;
import com.github.stashukvadim.revolut.transfer.model.Transfer;
import com.github.stashukvadim.revolut.transfer.model.TransferResult;
import com.github.stashukvadim.revolut.transfer.repository.AccountRepository;
import com.github.stashukvadim.revolut.transfer.repository.AccountTablePopulator;
import com.github.stashukvadim.revolut.transfer.repository.TransactionUtil;
import com.github.stashukvadim.revolut.transfer.testutil.TransferTestUtil;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class TransferServiceTestIT {
    private TransactionUtil transactionUtil = TransferTestUtil.getTransactionUtil();
    private AccountRepository accountRepo = new AccountRepository(transactionUtil);
    private TransferService transferService = new TransferService(accountRepo, transactionUtil);
    private AccountTablePopulator accountTablePopulator = new AccountTablePopulator(transactionUtil);

    @Before
    public void setUp() {
        accountTablePopulator.createAccountsTableIfNotExists();
        accountTablePopulator.deleteAllAccounts();
    }

    @Test
    public void testTransfer() {
        accountTablePopulator.addAccount(1, 1_000_000);
        accountTablePopulator.addAccount(2, 2_000_000);

        Transfer transfer = new Transfer(1, 2, 1_000_000);

        TransferResult transferResult = transferService.transfer(transfer);

        assertThat(transferResult.getTransfer()).isEqualTo(transfer);
        assertThat(transferResult.getFromAccountBalanceMicrosAfterTransfer()).isEqualTo(0);
        assertThat(accountRepo.getAccount(1).get().getBalanceMicros()).isEqualTo(0);
        assertThat(accountRepo.getAccount(2).get().getBalanceMicros()).isEqualTo(3_000_000);
    }

    @Test
    public void testMultipleTransfers() {
        accountTablePopulator.addAccount(1, 1_000_000);
        accountTablePopulator.addAccount(2, 2_000_000);

        transferService.transfer(new Transfer(1, 2, 1_000_000));
        assertThat(accountRepo.getAccount(1).get().getBalanceMicros()).isEqualTo(0);
        assertThat(accountRepo.getAccount(2).get().getBalanceMicros()).isEqualTo(3_000_000);

        transferService.transfer(new Transfer(2, 1, 2_000_000));
        assertThat(accountRepo.getAccount(1).get().getBalanceMicros()).isEqualTo(2_000_000);
        assertThat(accountRepo.getAccount(2).get().getBalanceMicros()).isEqualTo(1_000_000);
    }

    @Test
    public void testTransferWithNonExistentAccounts() {
        assertThatExceptionOfType(TransferException.class)
                .isThrownBy(() ->
                        transferService.transfer(new Transfer(1111, 2222, 1_000_000)))
                .withMessage("Account with id 1111 does not exist.");

    }

    @Test
    public void testTransferWithInsufficientFunds() {
        accountTablePopulator.addAccount(1, 1_000_000);
        accountTablePopulator.addAccount(2, 2_000_000);

        assertThatExceptionOfType(TransferException.class)
                .isThrownBy(() -> transferService.transfer(new Transfer(1, 2, 1_000_001)))
                .withMessage("Account with id 1 doesn't have enough money to make the transfer.");

        assertThat(accountRepo.getAccount(1).get().getBalanceMicros()).isEqualTo(1_000_000);
        assertThat(accountRepo.getAccount(2).get().getBalanceMicros()).isEqualTo(2_000_000);
    }

    @Test
    public void testTransferWithoutOverflow() {
        accountTablePopulator.addAccount(1, 1_000_000);
        accountTablePopulator.addAccount(2, Long.MAX_VALUE - 1);

        transferService.transfer(new Transfer(1, 2, 1));

        assertThat(accountRepo.getAccount(1).get().getBalanceMicros()).isEqualTo(1_000_000 - 1);
        assertThat(accountRepo.getAccount(2).get().getBalanceMicros()).isEqualTo(Long.MAX_VALUE);
    }

    @Test
    public void testTransferWithOverflow() {
        accountTablePopulator.addAccount(1, 1_000_000);
        accountTablePopulator.addAccount(2, Long.MAX_VALUE);

        assertThatExceptionOfType(TransferException.class)
                .isThrownBy(() ->
                        transferService.transfer(new Transfer(1, 2, 1)))
                .withMessage("toAccount can't accept this transaction.");

        assertThat(accountRepo.getAccount(1).get().getBalanceMicros()).isEqualTo(1_000_000);
        assertThat(accountRepo.getAccount(2).get().getBalanceMicros()).isEqualTo(Long.MAX_VALUE);
    }


    @Test
    public void testTransferWithInsufficientFundsAndThenWithSmallerAmount() {
        accountTablePopulator.addAccount(1, 1_000_000);
        accountTablePopulator.addAccount(2, 2_000_000);

        assertThatExceptionOfType(TransferException.class)
                .isThrownBy(() ->
                        transferService.transfer(new Transfer(1, 2, 1_000_001)))
                .withMessage("Account with id 1 doesn't have enough money to make the transfer.");

        transferService.transfer(new Transfer(1, 2, 1_000_000));
        assertThat(accountRepo.getAccount(1).get().getBalanceMicros()).isEqualTo(0);
        assertThat(accountRepo.getAccount(2).get().getBalanceMicros()).isEqualTo(3_000_000);
    }
}
