package com.github.stashukvadim.revolut.transfer.testutil;

import com.github.stashukvadim.revolut.transfer.repository.TransactionUtil;

public class TransferTestUtil {
    private static final String TEST_DB_URL = "jdbc:h2:mem:example_db;DB_CLOSE_DELAY=-1";

    public static TransactionUtil getTransactionUtil() {
        return new TransactionUtil(TEST_DB_URL);
    }
}
