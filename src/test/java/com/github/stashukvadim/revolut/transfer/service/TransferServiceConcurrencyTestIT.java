package com.github.stashukvadim.revolut.transfer.service;

import com.github.stashukvadim.revolut.transfer.TransferException;
import com.github.stashukvadim.revolut.transfer.model.Transfer;
import com.github.stashukvadim.revolut.transfer.repository.AccountRepository;
import com.github.stashukvadim.revolut.transfer.repository.AccountTablePopulator;
import com.github.stashukvadim.revolut.transfer.repository.TransactionUtil;
import com.github.stashukvadim.revolut.transfer.testutil.TransferTestUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class TransferServiceConcurrencyTestIT {
    private static final long ONE_MILLION_MICROS = 1_000_000L;
    private static final long ONE_HUNDRED_MICROS = 100L;
    private TransactionUtil transactionUtil = TransferTestUtil.getTransactionUtil();
    private AccountRepository accountRepo = new AccountRepository(transactionUtil);
    private TransferService transferService = new TransferService(accountRepo, transactionUtil);
    private AccountTablePopulator accountTablePopulator = new AccountTablePopulator(transactionUtil);

    @Before
    public void setUp() {
        accountTablePopulator.createAccountsTableIfNotExists();
        accountTablePopulator.deleteAllAccounts();
    }

    @Test
    public void testConcurrencyTransferBetweenTwoAccounts() {
        long transfersCount = 10_000;
        accountTablePopulator.addAccount(1, ONE_MILLION_MICROS);
        accountTablePopulator.addAccount(2, 0);

        List<Transfer> transfers = new Random().longs(transfersCount, 1, ONE_HUNDRED_MICROS)
                .mapToObj(i -> new Transfer(1, 2, i))
                .collect(Collectors.toList());

        long start = System.currentTimeMillis();

        long expectedTransferredSum = 0;
        for (Transfer transfer : transfers) {
            expectedTransferredSum += transfer.getAmountMicros();
        }

        //when
        transfers.parallelStream().forEach(transferService::transfer);

        System.out.println(String.format("It took %s ms to process %s transfers", System.currentTimeMillis() - start, transfersCount));

        //then
        assertThat(accountRepo.getAccount(1).get().getBalanceMicros()).isEqualTo(ONE_MILLION_MICROS - expectedTransferredSum);
        assertThat(accountRepo.getAccount(2).get().getBalanceMicros()).isEqualTo(expectedTransferredSum);
    }

    @Test
    public void testConcurrencyTransferBetweenTwoAccountsDeadLock() {
        long transfersCount = 1000;
        accountTablePopulator.addAccount(1, ONE_MILLION_MICROS);
        accountTablePopulator.addAccount(2, ONE_MILLION_MICROS);

        List<Transfer> transfers = new ArrayList<>();
        for (int i = 0; i < transfersCount; i++) {
            transfers.add(new Transfer(1, 2, 100));
            transfers.add(new Transfer(2, 1, 200));
        }

        long expectedAccount1BalanceAfterTransfers = ONE_MILLION_MICROS + transfersCount * 100;
        long expectedAccount2BalanceAfterTransfers = ONE_MILLION_MICROS - transfersCount * 100;

        //transfer in parallel money from account 1 to account 2 AND from account 2 to account 1
        //this can cause deadlock if transferring not correctly implemented
        transfers.parallelStream().forEach(transferService::transfer);

        //then
        assertThat(accountRepo.getAccount(1).get().getBalanceMicros()).isEqualTo(expectedAccount1BalanceAfterTransfers);
        assertThat(accountRepo.getAccount(2).get().getBalanceMicros()).isEqualTo(expectedAccount2BalanceAfterTransfers);
    }

    @Test
    public void testConcurrencyTransferBetweenTwoAccountsInsufficientFunds() {
        accountTablePopulator.addAccount(1, 1);
        accountTablePopulator.addAccount(2, 0);

        //multiple transfers to transfer 1 micros from account 1 which has only 1 micros on balance
        //only one of these transfers should be successful
        List<Transfer> transfers = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            transfers.add(new Transfer(1, 2, 1));
        }

        assertThatExceptionOfType(TransferException.class)
                .isThrownBy(() -> transfers.parallelStream().forEach(transferService::transfer))
                .withMessage("Account with id 1 doesn't have enough money to make the transfer.")
        ;

        //then only one transaction was successful resulting in correct balance
        assertThat(accountRepo.getAccount(1).get().getBalanceMicros()).isEqualTo(0);
        assertThat(accountRepo.getAccount(2).get().getBalanceMicros()).isEqualTo(1);
    }
}
