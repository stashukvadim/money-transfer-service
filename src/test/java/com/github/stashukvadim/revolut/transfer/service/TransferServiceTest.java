package com.github.stashukvadim.revolut.transfer.service;

import com.github.stashukvadim.revolut.transfer.TransferException;
import com.github.stashukvadim.revolut.transfer.model.Account;
import com.github.stashukvadim.revolut.transfer.model.Transfer;
import com.github.stashukvadim.revolut.transfer.model.TransferResult;
import com.github.stashukvadim.revolut.transfer.repository.AccountRepository;
import com.github.stashukvadim.revolut.transfer.testutil.TransferTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

public class TransferServiceTest {
    private AccountRepository accountRepository;
    private TransferService transferService;

    @Before
    public void setUp() {
        accountRepository = mock(AccountRepository.class);
        transferService = new TransferService(accountRepository, TransferTestUtil.getTransactionUtil());
    }

    @Test
    public void shouldThrowNullPointerExceptionWhenTransferIsNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(() ->
                transferService.transfer(null)
        );

        verifyZeroInteractions(accountRepository);
    }

    @Test
    public void shouldThrowExceptionWhenAccountRepositoryThrowsException() {
        when(accountRepository.getAccount(1)).thenThrow(new RuntimeException("Something bad happened"));
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() ->
                transferService.transfer(new Transfer(1, 2, 1)
                )).withMessage("java.lang.RuntimeException: Something bad happened");
    }

    @Test
    public void shouldThrowTransferExceptionWhenAmountIsZero() {
        assertThatExceptionOfType(TransferException.class).isThrownBy(() ->
                transferService.transfer(new Transfer(1, 2, 0))
        ).withMessage("Amount should be greater than 0. Actual value = 0.");
    }

    @Test
    public void shouldThrowTransferExceptionWhenAmountIsLessThanZero() {
        assertThatExceptionOfType(TransferException.class).isThrownBy(() ->
                transferService.transfer(new Transfer(1, 2, -1))
        ).withMessage("Amount should be greater than 0. Actual value = -1.");
    }

    @Test
    public void shouldThrowTransferExceptionWhenFromAccountDoesNotExist() {
        when(accountRepository.getAccount(1)).thenReturn(Optional.empty());
        assertThatExceptionOfType(TransferException.class).isThrownBy(() ->
                transferService.transfer(new Transfer(1, 2, 1))
        ).withMessage("Account with id 1 does not exist.");
    }

    @Test
    public void shouldThrowTransferExceptionWhenToAccountDoesNotExist() {
        when(accountRepository.getAccount(1)).thenReturn(Optional.of(new Account(1, 1)));
        when(accountRepository.getAccount(2)).thenReturn(Optional.empty());
        assertThatExceptionOfType(TransferException.class).isThrownBy(() ->
                transferService.transfer(new Transfer(1, 2, 1))
        ).withMessage("Account with id 2 does not exist.");
    }

    @Test
    public void shouldThrowTransferExceptionWhenFromAccountHasInsufficientFunds() {
        when(accountRepository.getAccount(1)).thenReturn(Optional.of(new Account(1, 1)));
        when(accountRepository.getAccount(2)).thenReturn(Optional.of(new Account(2, 0)));
        assertThatExceptionOfType(TransferException.class).isThrownBy(() ->
                transferService.transfer(new Transfer(1, 2, 2))
        ).withMessage("Account with id 1 doesn't have enough money to make the transfer.");
    }

    @Test
    public void shouldThrowTransferExceptionWhenFromAccountAndToAccountAreTheSame() {
        when(accountRepository.getAccount(1)).thenReturn(Optional.of(new Account(1, 1)));
        assertThatExceptionOfType(TransferException.class).isThrownBy(() ->
                transferService.transfer(new Transfer(1, 1, 1))
        ).withMessage("Transfer between the same account is not allowed.");
    }

    /**
     * This is very unlikely to happen, as the maximum balance supported is around 9 trillion dollars (or other currency)
     */
    @Test
    public void shouldThrowTransferExceptionWhenToAmountBalanceAfterTransactionCausesLongOverflow() {
        when(accountRepository.getAccount(1)).thenReturn(Optional.of(new Account(1, 1)));
        when(accountRepository.getAccount(2)).thenReturn(Optional.of(new Account(2, Long.MAX_VALUE)));
        assertThatExceptionOfType(TransferException.class).isThrownBy(() ->
                transferService.transfer(new Transfer(1, 2, 1))
        ).withMessage("toAccount can't accept this transaction.");
    }

    @Test
    public void successfulTransferFromLowerAccountIdToHigherAccountId() {
        when(accountRepository.getAccount(1)).thenReturn(Optional.of(new Account(1, 5)));
        when(accountRepository.getAccount(2)).thenReturn(Optional.of(new Account(2, 0)));
        Transfer transfer = new Transfer(1, 2, 1);

        //when
        TransferResult transferResult = transferService.transfer(transfer);

        //then transfer result is valid
        assertThat(transferResult.getTransfer()).isEqualTo(transfer);
        assertThat(transferResult.getFromAccountBalanceMicrosAfterTransfer()).isEqualTo(4);

        //then transfer account repository was called in correct order
        // (smaller accountId called first to prevent the possibility of deadlock)
        InOrder inOrder = inOrder(accountRepository);
        inOrder.verify(accountRepository).getAccount(1);
        inOrder.verify(accountRepository).getAccount(2);

        //then accountRepository was called with correct addAmountMicros arguments
        verify(accountRepository, times(1)).addAmountMicros(1, -1);
        verify(accountRepository, times(1)).addAmountMicros(2, 1);
    }

    @Test
    public void successfulTransferFromHigherAccountIdToLowerAccountId() {
        when(accountRepository.getAccount(1)).thenReturn(Optional.of(new Account(1, 5)));
        when(accountRepository.getAccount(2)).thenReturn(Optional.of(new Account(2, 2)));
        Transfer transfer = new Transfer(2, 1, 1);

        //when
        TransferResult transferResult = transferService.transfer(transfer);

        //then transfer result is valid
        assertThat(transferResult.getTransfer()).isEqualTo(transfer);
        assertThat(transferResult.getFromAccountBalanceMicrosAfterTransfer()).isEqualTo(1);

        //then transfer account repository was called in correct order
        // (smaller accountId called first to prevent the possibility of deadlock)
        InOrder inOrder = inOrder(accountRepository);
        inOrder.verify(accountRepository).getAccount(1);
        inOrder.verify(accountRepository).getAccount(2);

        //then accountRepository was called with correct addAmountMicros arguments
        verify(accountRepository, times(1)).addAmountMicros(2, -1);
        verify(accountRepository, times(1)).addAmountMicros(1, 1);
    }
}